package com.example.lucas.dao;

/**
 * Created by Lucas on 16/04/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DaoUsuario extends SQLiteOpenHelper {

    public static final String NOME_TABELA = "usuario";
    public static final String ID = "id";
    public static final String COLUNA1 = "nome";
    public static final String COLUNA2 = "nome_usuario";
    public static final String COLUNA3 = "id";
    public static final String COLUNAS = "nome,nome_usuario";
    public static final String[] COLUNAS_ARRAY = {"id","nome","nome_usuario"};
    public static final String NOME_BD = "AppListaExercicio.db";
    public static final int VERSAO_BD = 1;
    public static final String CRIACAO_TABELA = "create table if not exists "+NOME_TABELA+
            "("+ID+" integer primary key "+","+
            COLUNA1+" text"+","+
            COLUNA2+" text"+" );";
    private SQLiteDatabase db;

    public DaoUsuario(Context context) {
        super(context, NOME_BD, null, VERSAO_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CRIACAO_TABELA);
    }

    public void inserir(String nome,String nomeColuna,String id){
        ContentValues colunasComConteudoDoInsert = new ContentValues();
        colunasComConteudoDoInsert.put(COLUNA1,nome);
        colunasComConteudoDoInsert.put(COLUNA2,nomeColuna);
        colunasComConteudoDoInsert.put(COLUNA3,id);

        abrirConexaoParaEscrita();
        this.db.insert(NOME_TABELA,null,colunasComConteudoDoInsert);
        fecharConexao();
        Log.w("inserir","inseriu no banco de dados");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void deletar(){
        String dropTable = "drop table if exists "+NOME_TABELA;
        Log.w("deletar ",dropTable);
        abrirConexaoParaEscrita();
        this.db.execSQL(dropTable);
        fecharConexao();
    }

    public void detelarUsuario(){
        Log.i("detelarUsuario","deletou");
        abrirConexaoParaEscrita();
        this.db.delete(NOME_TABELA,null,null);
        fecharConexao();
    }

    public void updateDadoUsuario(String nome, String nomeApelido){
        abrirConexaoParaEscrita();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUNA1,nome);
        contentValues.put(COLUNA2,nomeApelido);
        this.db.update("usuario",contentValues,null,null);
        fecharConexao();
    }

    public void criar(){
        abrirConexaoParaEscrita();
        onCreate(this.db);
        fecharConexao();
    }

    public void abrirConexaoParaEscrita(){
        this.db = this.getWritableDatabase();
    }

    public void abrirConexaoParaLeitura() {
        this.db = this.getReadableDatabase();
    }

    public void fecharConexao(){
        this.db.close();
    }


    private Cursor consultaUsuario(){
        abrirConexaoParaLeitura();
        Cursor cursor = this.db.query(NOME_TABELA,COLUNAS_ARRAY,null,null,null,null,null,null);
        Log.i("consultauuario = ","teste");
        Log.i("cursor",String.valueOf(cursor.getCount()));
        fecharConexao();

        if(cursor!= null){
            if(cursor.getCount() != 0) {
                Log.i("consultaUsuario", "dentro do if");
                cursor.moveToFirst();
                Log.i("consultar = ", cursor.getString(0));
                return cursor;
            }
        }
        return null;
    }

    public String idDoUsuario(){
       // inserir();
        abrirConexaoParaLeitura();
        Cursor cursor = this.db.query(NOME_TABELA,COLUNAS_ARRAY,null,null,null,null,null,null);
        if(cursor!= null){
            cursor.moveToFirst();
            Log.i("idDoUsuario","vamos ver o valor");
            //Log.i("idDoUsuario",cursor.getString(0));
            String id ="nao tem nada";
            try {
               if(cursor.isNull(cursor.getColumnIndex("id"))) {
                   id="esta tudo vazio";
               }else{
                   id = cursor.getString(0);
               }
            }catch (Exception e){
                e.printStackTrace();
            }
            Log.i("id=",id);
            Log.i("idDoUsuario","antes do retorno");
            return id;
        }
        Log.i("idDoUsuario","vai retornar null");
        fecharConexao();
        return null;
    }



    public boolean usuarioTemId(){
        if(consultaUsuario()!= null){
            return true;
        }else{
            return false;
        }
    }

    public String[] obterDadosUsuario(){
        String[] dadosDoUsuario = new String[3];
        abrirConexaoParaLeitura();
        Cursor cursor = this.db.query(NOME_TABELA,COLUNAS_ARRAY,null,null,null,null,null,null);

        if(cursor!= null){
            cursor.moveToFirst();
            Log.i("idDoUsuario","vamos ver o valor");
            //Log.i("idDoUsuario",cursor.getString(0));
            String id ="nao tem nada";
            try {
                if(cursor.getCount() != 0){
                    dadosDoUsuario[0] = cursor.getString(0);
                    dadosDoUsuario[1] = cursor.getString(1);
                    dadosDoUsuario[2] = cursor.getString(2);
                }

                if(cursor.isNull(cursor.getColumnIndex("id"))) {
                    id="esta tudo vazio";
                }else{
                    id = cursor.getString(0);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            Log.i("id=",id);
            Log.i("idDoUsuario","antes do retorno");
            fecharConexao();
            return dadosDoUsuario;
        }
        Log.i("idDoUsuario","vai retornar null");
        fecharConexao();
        return null;
    }

}
