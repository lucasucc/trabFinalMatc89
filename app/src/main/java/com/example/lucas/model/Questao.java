package com.example.lucas.model;

import java.io.File;
import java.util.List;

/**
 * Created by Lucas on 15/05/2016.
 */
public class Questao {

    private String tag;
    private String idLista;
    private File imagem;
    private String nomeDoArquivo;

    public String getIdQuestao() {
        return idQuestao;
    }

    public void setIdQuestao(String idQuestao) {
        this.idQuestao = idQuestao;
    }

    public String getIdLista() {
        return idLista;
    }

    public void setIdLista(String idLista) {
        this.idLista = idLista;
    }

    private String idQuestao;

    public Questao() {

    }

    public Questao(String tag, File imagem, String nomeDoArquivo,String idLista){
        this.tag = tag;
        this.imagem = imagem;
        this.nomeDoArquivo = nomeDoArquivo;
        this.idLista = idLista;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setImagem(File imagem) {
        this.imagem = imagem;
    }

    public void setNomeDoArquivo(String nomeDoArquivo) {
        this.nomeDoArquivo = nomeDoArquivo;
    }

    public String getTag() {
        return tag;
    }

    public File getImagem() {
        return imagem;
    }

    public String getNomeDoArquivo() {
        return nomeDoArquivo;
    }
}
