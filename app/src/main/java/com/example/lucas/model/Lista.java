package com.example.lucas.model;

import java.io.File;

/**
 * Created by Lucas on 21/05/2016.
 */
public class Lista {


    String titulo;
    File imagem;
    String nomeDoArquivo;
    String idLista;

    public Lista(String titulo, File imagem, String nomeDoArquivo,String idLista) {
        this.titulo = titulo;
        this.imagem = imagem;
        this.nomeDoArquivo = nomeDoArquivo;
        this.idLista = idLista;
    }

    public Lista() {

    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public File getImagem() {
        return imagem;
    }

    public void setImagem(File imagem) {
        this.imagem = imagem;
    }

    public String getNomeDoArquivo() {
        return nomeDoArquivo;
    }

    public void setNomeDoArquivo(String nomeDoArquivo) {
        this.nomeDoArquivo = nomeDoArquivo;
    }

    public String getIdLista() {
        return idLista;
    }

    public void setIdLista(String idLista) {
        this.idLista = idLista;
    }
}
