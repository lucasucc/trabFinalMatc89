package com.example.lucas.model;

/**
 * Created by Lucas on 17/04/2016.
 */
public class Usuario {

    private int id;
    private String nome;
    private String nome_user;


    public Usuario() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome_user() {
        return nome_user;
    }

    public void setNome_user(String nome_user) {
        this.nome_user = nome_user;
    }
}
