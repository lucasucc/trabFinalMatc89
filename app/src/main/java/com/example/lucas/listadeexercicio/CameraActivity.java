package com.example.lucas.listadeexercicio;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.File;

public class CameraActivity extends AppCompatActivity {

    private static String pastaPadraoConteudo ="imagem_conteudo";
    private static String pastaPadrao ="imagem_app";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chamarCamera3();
        setContentView(R.layout.activity_activity_camera);
        //finish();
    }


    public void chamarCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File pastaPadraoSalvamentoDaImagem = new File(Environment.getExternalStorageDirectory(), pastaPadraoConteudo+"/ListaDeExercicio");
        //Uri fileUri = getOutputMediaFileUri(MediaStore.MEDIA_TYPE_IMAGE);
        pastaPadraoSalvamentoDaImagem.mkdirs();
        String nomeDoArquivo = String.valueOf("imagem"+System.currentTimeMillis()+".jpg");
        String caminhoCompletoArquivo = Environment.getExternalStorageDirectory()+pastaPadraoConteudo+"/"+nomeDoArquivo;
        File imagem = new File(pastaPadraoSalvamentoDaImagem,nomeDoArquivo);
        Uri uriSavedImage = Uri.fromFile(imagem);
        Bundle conteudoArquivo = new Bundle();
        conteudoArquivo.putString("nomeArquivo",nomeDoArquivo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        //intent.putExtra("conteudoArquivo",conteudoArquivo);
        //setResult(1, intent);
        //finish();

        //startActivityForResult(intent,0);

        //intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);


    }


    public void chamarCamera2(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String caminhoParcial = String.valueOf("ListaDeExercicio/"+pastaPadraoConteudo);
        Log.i("caminhoParcial",caminhoParcial);
        File pastaPadraoSalvamentoDaImagem = new File(Environment.getExternalStorageDirectory(), "ListaDeExercicio/"+pastaPadraoConteudo);
        //Uri fileUri = getOutputMediaFileUri(MediaStore.MEDIA_TYPE_IMAGE);
        pastaPadraoSalvamentoDaImagem.mkdirs();
        String nomeDoArquivo = String.valueOf("imagem"+System.currentTimeMillis()+".jpg");
        File imagem = new File(pastaPadraoSalvamentoDaImagem, nomeDoArquivo);
        Uri uriSavedImage = Uri.fromFile(imagem);

        Bundle conteudoArquivo = new Bundle();
        conteudoArquivo.putString("nomeArquivo",nomeDoArquivo);
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        intent.putExtra("nomeArquivo",conteudoArquivo);
        //startActivityForResult(intent,0);
        //setResult(1, intent);
        //finish();
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
    }

   public void chamarCamera3(){
       Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
       File pastaPadraoSalvamentoDaImagem = new File(Environment.getExternalStorageDirectory(), "ListaDeExercicio/"+pastaPadraoConteudo);
       //Uri fileUri = getOutputMediaFileUri(MediaStore.MEDIA_TYPE_IMAGE);
       pastaPadraoSalvamentoDaImagem.mkdirs();

       File imagem = new File(pastaPadraoSalvamentoDaImagem, String.valueOf("imagem"+System.currentTimeMillis()+".jpg"));
       Uri uriSavedImage = Uri.fromFile(imagem);

       intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
       //startActivityForResult(intent,0);

       //intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
   }

}
