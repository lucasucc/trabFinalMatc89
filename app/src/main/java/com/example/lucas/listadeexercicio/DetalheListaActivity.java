package com.example.lucas.listadeexercicio;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lucas.model.Lista;
import com.example.lucas.model.Questao;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xml.sax.helpers.LocatorImpl;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;

public class DetalheListaActivity extends AppCompatActivity {

    private ListView questoesView;
    private List<Questao> questoes;
    private List questoesItens;
    ArrayAdapter<String> questoesArrayAdapter;
    private String idLista;
    private Path pathsDoApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_lista);
        Intent intent = getIntent();
        this.idLista = intent.getExtras().getString("idLista");
        Log.i("onCreate idLista=",this.idLista);
        this.pathsDoApp = new Path();

        this.questoes = new ArrayList<Questao>();
        this.questoesItens = new ArrayList<String>();
        this.questoesArrayAdapter = new ArrayAdapter<String>(this,R.layout.item_listview_questao,R.id.txtTagsQuestoes,questoesItens);
        this.questoesView = (ListView)findViewById(R.id.listView3);

        this.questoesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                chamarDetalheQuestao(view);
            }
        });

        this.questoesView.setAdapter(questoesArrayAdapter);
        MyAsyncTaskQuestoesLista tasksQuestao = new MyAsyncTaskQuestoesLista();
        tasksQuestao.execute(this.idLista);
    }

    public void criarObjetosQuestao(String[] listasDoServico ){
        for ( String listaDoServico:listasDoServico) {
            String[] StringPosicao = listaDoServico.split(";");
            Questao questao = new Questao();
            questao.setIdLista(idLista);
            questao.setIdQuestao(StringPosicao[0]);
            questao.setImagem( gerarImagem( StringPosicao[1],questao.getIdQuestao()));
            /*
            lista.setIdLista(StringPosicao[0].replace("[","").trim());
            lista.setTitulo(StringPosicao[1].trim());
            lista.setImagem( gerarImagem( StringPosicao[2],questao.getIdLista()));
            lista.setNomeDoArquivo(lista.getIdLista()+".jpg");
            */
            atualizarListas(questao);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public File gerarImagem(String base64, String idFotografia){
        byte[] imagemByte = Base64.decode(base64,Base64.DEFAULT);
        File pasta = new File(this.pathsDoApp.getCaminhoCompletoAtePastaPadrao());
        pasta.mkdirs();
        File file = new File(this.pathsDoApp.getCaminhoCompletoAtePastaPadrao() +"/"+ idFotografia + ".jpg");
        try (OutputStream imageOut = new FileOutputStream(file.getPath()) ){
            imageOut.write(imagemByte);
            imageOut.close();

            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void atualizarListas(Questao questao){
        questoes.add(questao);
        questoesItens.add("Questão: "+this.questoes.size());
        questoesArrayAdapter.notifyDataSetChanged();
    }

    public void limparTodasListas(){
        questoes.clear();
        questoesItens.clear();
        questoesArrayAdapter.clear();
        questoesArrayAdapter.notifyDataSetChanged();
    }


    public void chamarDetalheQuestao(View view){
        Log.i("chamarDetalheQuestao","funcionou");
        Intent intent = new Intent(this,DetalheQuestaoActivity.class);
        startActivity(intent);
    }

    private class MyAsyncTaskQuestoesLista extends AsyncTask<String, Void, String> {

        private static final String NAMESPACE = "http://service/";
        private static final String MAIN_REQUEST_URL = "http://192.168.43.24:8082/WebServerListaExercicio/services/ServicePort";
        //private static final String METHOD_NAME = "multiplicar";
        //private static final String ARG_NAME0 = "arg0";
        //private static final String ARG_NAME1 = "arg1";

        @Override
        protected void onPreExecute() {
            //progressBar2.setVisibility(View.VISIBLE);
            limparTodasListas();
        }

        @Override
        protected String doInBackground(String... params) {
            Log.i("nome do metodo =",params[0]);
            SoapObject soapObject = new SoapObject(NAMESPACE, "consultaQuestoesListaTag");

            soapObject.addProperty("arg0",params[0]);
            soapObject.addProperty("arg1","");

            HttpTransportSE httpTransport = getHttpTransportSE();

            SoapSerializationEnvelope envelope = getSoapSerializationEnvelope(soapObject);

            try {
                httpTransport.call("", envelope);

                return envelope.getResponse().toString();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            //progressBar2.setVisibility(View.INVISIBLE);
            Log.i("AULA", "#############################");
            Log.i("AULA", response);
            Log.i("AULA", "#############################");

            Toast.makeText(getApplicationContext(),"Questões da lista carregada",Toast.LENGTH_LONG).show();
            //chamarDetalheLista();
            criarObjetosQuestao(response.split("]"));
            //atualizarListas(questao);
        }

        private final SoapSerializationEnvelope getSoapSerializationEnvelope(SoapObject soapObject) {

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.implicitTypes = true;
            envelope.setAddAdornments(false);

            envelope.setOutputSoapObject(soapObject);

            return envelope;
        }

        private final HttpTransportSE getHttpTransportSE() {

            HttpTransportSE ht = new HttpTransportSE(Proxy.NO_PROXY, MAIN_REQUEST_URL, 60000);
            ht.debug = true;
            ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");

            return ht;
        }
    }
}
