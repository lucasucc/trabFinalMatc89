package com.example.lucas.listadeexercicio;

import android.os.Environment;

/**
 * Created by Lucas on 14/05/2016.
 */
public class Path {

    private String pastaPadraoConteudo;
    private String pastaPadrao;
    private String nomePastaApp;
    private String caminhoCompletoAtePastaApp;
    private String caminhoCompletoAtePastaPadraoConteudo;
    private String caminhoCompletoAtePastaPadrao;
    private String nomeIconePadraoImagemPrincipal;
    private String caminhoParaIconeImagemPrincipal;


    public Path() {
        this.pastaPadraoConteudo = "imagem_conteudo";
        this.pastaPadrao = "imagem_app";
        this.nomePastaApp = "ListaDeExercicio";
        this.caminhoCompletoAtePastaApp = Environment.getExternalStorageDirectory()+"/"+ this.nomePastaApp;
        this.caminhoCompletoAtePastaPadrao =  this.caminhoCompletoAtePastaApp+"/"+ this.pastaPadrao;
        this.caminhoCompletoAtePastaPadraoConteudo =  this.caminhoCompletoAtePastaApp+"/"+ this.pastaPadraoConteudo;
        this.nomeIconePadraoImagemPrincipal = "icone_vazio.png";
        this.caminhoParaIconeImagemPrincipal = caminhoCompletoAtePastaPadrao+"/"+this.nomeIconePadraoImagemPrincipal;
    }

    public String getCaminhoParaIconeImagemPrincipal() {
        return caminhoParaIconeImagemPrincipal;
    }

    public String getPastaPadraoConteudo() {
        return pastaPadraoConteudo;
    }

    public String getPastaPadrao() {
        return pastaPadrao;
    }

    public String getNomePastaApp() {
        return nomePastaApp;
    }

    public String getCaminhoCompletoAtePastaApp() {
        return caminhoCompletoAtePastaApp;
    }

    public String getCaminhoCompletoAtePastaPadraoConteudo() {
        return caminhoCompletoAtePastaPadraoConteudo;
    }

    public String getCaminhoCompletoAtePastaPadrao() {
        return caminhoCompletoAtePastaPadrao;
    }

    public String getNomeIconePadraoImagemPrincipal() {
        return nomeIconePadraoImagemPrincipal;
    }
}
