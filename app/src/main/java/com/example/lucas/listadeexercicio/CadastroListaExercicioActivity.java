package com.example.lucas.listadeexercicio;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lucas.dao.DaoUsuario;
import com.example.lucas.model.Questao;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;

public class CadastroListaExercicioActivity extends AppCompatActivity {

    private ImageView imageView;
    //private static String pastaPadraoConteudo ="imagem_conteudo";
   // private static String pastaPadrao ="imagem_app";
    //private List<String> nomeDasImagens;
    private String nomeDaImagemPrincipal;
    private Path pathsDoApp;
    private ListView questoesView;
    private ArrayAdapter<String> questoesItens;
    private List<String> questoesOrdem;
    private List<Questao> questoes;
    private EditText editTextTag;
    private DaoUsuario daoUsuario;
    private ProgressBar progressBar;
    private EditText titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_lista_exercicio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.pathsDoApp = new Path();
        this.imageView = (ImageView) findViewById(R.id.imageView);
        this.titulo = (EditText) findViewById(R.id.editText3);
        this.questoesOrdem = new ArrayList<String>();
        this.questoes = new ArrayList<Questao>();
        this.questoesItens = new ArrayAdapter<String>(this, R.layout.item_listview_questao, R.id.txtTagsQuestoes, this.questoesOrdem);
        this.questoesView = (ListView) findViewById(R.id.listView);
        this.questoesView.setAdapter(this.questoesItens);
        this.nomeDaImagemPrincipal = "imagemPadrao";
        this.editTextTag = (EditText)findViewById(R.id.editText4);
        this.daoUsuario = new DaoUsuario(this);
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cadastro, menu);
        return true;
    }

    public void exibirImagem(View view) {

        if( this.nomeDaImagemPrincipal =="imagemPadrao" ){
            Toast.makeText(this,"você não visualizar a imagem padrão",Toast.LENGTH_SHORT).show();
        }else{

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("file://" + this.nomeDaImagemPrincipal), "image/*");
            startActivity(intent);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //startActivityForResult(intent,1);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    public void removerFotoQuestao(View view) {
        if(this.questoes.size() > 0) {
            this.questoes.remove(this.questoes.size()-1);
            if(this.questoesOrdem.size() != 0) {
                this.questoesOrdem.remove(this.questoesOrdem.size() - 1);
                this.questoesItens.notifyDataSetChanged();
            }
        }else{
            Toast.makeText(this,"Não tem foto da questão para remover",Toast.LENGTH_SHORT).show();
        }
    }

    public void removerFotoPrincipal(View view){
        if( this.nomeDaImagemPrincipal == pathsDoApp.getCaminhoParaIconeImagemPrincipal()) {
            Toast.makeText(this,"Não tem foto da lista para remover",Toast.LENGTH_SHORT).show();
        }else{
            this.nomeDaImagemPrincipal = pathsDoApp.getCaminhoParaIconeImagemPrincipal();
            this.imageView.setImageResource(R.mipmap.icone_vazio);
        }
    }

    public void chamarActivityCameraQuestao(View view){
        if(this.editTextTag.getText().toString().trim().isEmpty()){
            Toast.makeText(this,"As tags da questão devem ser adicionadas antes da foto",Toast.LENGTH_LONG).show();
        }else {
            chamarCamera("questao");
        }
    }
    public void chamarActivityCamera(View view) {
        Log.i("camera","camera2");
        chamarCamera("principal");
        Log.i("chamarActivityCamera",this.nomeDaImagemPrincipal);

    }

    @Override
    public void onStart(){
        Log.i("onStart","onStart");

        super.onStart();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("onActivityResult","onActivityResult");
        Log.i("resultCode =",String.valueOf(resultCode));
        File fileNovaImagem;

        if (resultCode == -1) {
            if( requestCode == 100) {
                Log.i("if", "requestCode == resultCode");
                fileNovaImagem = new File(this.nomeDaImagemPrincipal);
                if(fileNovaImagem.exists()) {
                    this.imageView.setImageURI(Uri.fromFile(fileNovaImagem));
                }
            }else{
                fileNovaImagem = this.questoes.get(this.questoes.size()-1).getImagem();
                //EditText editTextTag = (EditText)findViewById(R.id.editText4);
                //Questao questao = new Questao(this.editTextTag.getText().toString(),fileNovaImagem,this.nomeDasImagens.get(this.nomeDasImagens.size()-1).toString());
                //this.questoes.add(questao);
                //editTextTag.setText(" ");
                if(fileNovaImagem.exists()){
                    this.questoesOrdem.add("questão "+questoes.size());
                    this.questoesItens.notifyDataSetChanged();
                }else{
                    this.questoes.remove(this.questoes.size()-1);
                    this.questoesOrdem.remove(this.questoesOrdem.size()-1);
                    this.questoesItens.notifyDataSetChanged();
                }

            }
        }
    }


    /*
    public void chamarCamera2(String tipoFoto){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String caminhoParcial = String.valueOf("ListaDeExercicio/"+pastaPadraoConteudo);
        Log.i("caminhoParcial",caminhoParcial);
        File pastaPadraoSalvamentoDaImagem = new File(Environment.getExternalStorageDirectory(), "ListaDeExercicio/"+pastaPadraoConteudo);
        pastaPadraoSalvamentoDaImagem.mkdirs();
        String nomeDoArquivo = String.valueOf("imagem"+System.currentTimeMillis()+".jpg");
        if(tipoFoto == "principal") {
           this.nomeDaImagemPrincipal=Environment.getExternalStorageDirectory()+"/ListaDeExercicio/"+pastaPadraoConteudo+"/"+nomeDoArquivo;
        }else if(tipoFoto == "questao"){
            this.nomeDasImagens.add(Environment.getExternalStorageDirectory()+"/ListaDeExercicio/"+pastaPadraoConteudo+"/"+nomeDoArquivo);
        }
        File imagem = new File(pastaPadraoSalvamentoDaImagem, nomeDoArquivo);
        Uri uriSavedImage = Uri.fromFile(imagem);

        //Bundle conteudoArquivo = new Bundle();
        //conteudoArquivo.putString("nomeArquivo",nomeDoArquivo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        //intent.putExtra("conteudoArquivo",conteudoArquivo);
        startActivityForResult(intent,100);

    }
    */

    public void chamarCamera(String tipoFoto){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        int numeroRetorno = 0;
        //String caminhoParcial = String.valueOf("ListaDeExercicio/"+pastaPadraoConteudo);
        //Log.i("caminhoParcial",caminhoParcial);
        File pastaPadraoSalvamentoDaImagem = new File(pathsDoApp.getCaminhoCompletoAtePastaPadraoConteudo());//new File(Environment.getExternalStorageDirectory(), "ListaDeExercicio/"+pastaPadraoConteudo);
        Log.i("caminhoParcial",pathsDoApp.getCaminhoCompletoAtePastaPadraoConteudo());
        pastaPadraoSalvamentoDaImagem.mkdirs();
        String nomeDoArquivo = String.valueOf("imagem"+System.currentTimeMillis()+".jpg");
        if(tipoFoto == "principal") {
            //this.nomeDaImagemPrincipal=Environment.getExternalStorageDirectory()+"/ListaDeExercicio/"+pastaPadraoConteudo+"/"+nomeDoArquivo;
            this.nomeDaImagemPrincipal=pathsDoApp.getCaminhoCompletoAtePastaPadraoConteudo()+"/"+nomeDoArquivo;
            numeroRetorno = 100;
        }else if(tipoFoto == "questao"){
            //this.nomeDasImagens.add(Environment.getExternalStorageDirectory()+"/ListaDeExercicio/"+pastaPadraoConteudo+"/"+nomeDoArquivo);
            Questao questao = new Questao();
            questao.setNomeDoArquivo(pathsDoApp.getCaminhoCompletoAtePastaPadraoConteudo()+"/"+nomeDoArquivo);
            questao.setImagem(new File(questao.getNomeDoArquivo()));

            questao.setTag(this.editTextTag.getText().toString());
            this.editTextTag.setText("");
            this.questoes.add(questao);
            //this.questoesOrdem.add("questao "+this.questoesOrdem.size());
            numeroRetorno = 101;
        }
        File imagem = new File(pathsDoApp.getCaminhoCompletoAtePastaPadraoConteudo(), nomeDoArquivo);
        Uri uriSavedImage = Uri.fromFile(imagem);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);

        startActivityForResult(intent,numeroRetorno);

    }

    public String fotoParaBase64(File file){

        File fotoPrincipal = file;
        int len = (int)fotoPrincipal.length();
        byte[] arrayFotoPrincipal = new byte[len];

        try {
            FileInputStream inFile = null;
            inFile = new FileInputStream(fotoPrincipal);
            inFile.read(arrayFotoPrincipal, 0, len);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(arrayFotoPrincipal,Base64.DEFAULT);
        //Log.i("teste=",Base64.encodeToString(arrayFotoPrincipal,Base64.DEFAULT));
    }

    public String[] tratarDados(){
        String resultado;
        String titulo;
        String fotoLista ="";
        Base64 base64Imagem;
        String[] dados = new String[4];
        //Log.i("testeeeee=",this.daoUsuario.idDoUsuario());
        dados[0] = this.daoUsuario.idDoUsuario();
        Log.i("id====",dados[0]);
        dados[1] = this.titulo.getText().toString();
        dados[2] = fotoParaBase64(new File(this.nomeDaImagemPrincipal));
        //Log.i("foto principal,base64= ",dados[1]);

        //Log.i("titulo=",dados[2]);
        for (Questao questao:this.questoes) {
            fotoLista = fotoLista+"["+ fotoParaBase64(questao.getImagem())+" "+questao.getTag()+"]";
            //fotoLista = fotoLista+"[tag "+questao.getTag()+"]";
            //Log.i("questao.getTag() = ",questao.getTag());
        }
        dados[3] = fotoLista;
        //Log.i("dados[3] = fotoLista",fotoLista);
        return dados;
    }

    public boolean validar(){
        Log.i("validar =",this.titulo.getText().toString());
        if(this.titulo.getText().toString().trim().length()== 0||this.nomeDaImagemPrincipal == "imagemPadrao" ||this.questoes.size() == 0 ){
            return false;
        }
        return true;
    }
    public void inserirLista(View view){

       // String fotoPrincipal = fotoParaBase64(new File(this.nomeDaImagemPrincipal));
       /*
        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute("msgTeste");

        */

        if(validar()) {
           String[] dados = tratarDados();
            MyAsyncTask myAsyncTask = new MyAsyncTask(this);
            myAsyncTask.execute(dados);
        }else{
            Toast.makeText(this,"Está faltando algo",Toast.LENGTH_SHORT).show();
        }
       // progressBar.setVisibility(View.INVISIBLE);
        //String titulo = this.
        /*
        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute("msgTeste");

        if(this.daoUsuario.usuarioTemId()){
            Log.i("inserir Lista","Tem usuário");
        }else{
            Log.i("inserir Lista","não tem usuário");
        }
        */
    }

    private class MyAsyncTask extends AsyncTask<String, Void, String> {

        private static final String NAMESPACE = "http://service/";
        private static final String MAIN_REQUEST_URL = "http://192.168.43.24:8082/WebServerListaExercicio/services/ServicePort";
        //private static final String METHOD_NAME = "multiplicar";
        //private static final String ARG_NAME0 = "arg0";
        //private static final String ARG_NAME1 = "arg1";
        private Context context;


        private MyAsyncTask(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            Log.i("nome do metodo =",params[0]);
            SoapObject soapObject = new SoapObject(NAMESPACE, "cadastroLista");

            soapObject.addProperty("arg0",params[0]);
            soapObject.addProperty("arg1",params[1]);
            soapObject.addProperty("arg2",params[2]);
            soapObject.addProperty("arg3",params[3]);

            /*
            if( obterArg0() == null && obterArg1() == null) {
                return null;
            }
            soapObject.addProperty(ARG_NAME0,obterArg0());
            soapObject.addProperty(ARG_NAME1,obterArg1());
            */
            HttpTransportSE httpTransport = getHttpTransportSE();

            SoapSerializationEnvelope envelope = getSoapSerializationEnvelope(soapObject);

            try {
                httpTransport.call("", envelope);

                return envelope.getResponse().toString();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            progressBar.setVisibility(View.INVISIBLE);
            Log.i("AULA", "#############################");
            Log.i("AULA", response);
            Log.i("AULA", "#############################");

            Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this.context,MainActivity.class);
            startActivity(intent);
        }

        private final SoapSerializationEnvelope getSoapSerializationEnvelope(SoapObject soapObject) {

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.implicitTypes = true;
            envelope.setAddAdornments(false);

            envelope.setOutputSoapObject(soapObject);

            return envelope;
        }

        private final HttpTransportSE getHttpTransportSE() {

            HttpTransportSE ht = new HttpTransportSE(Proxy.NO_PROXY, MAIN_REQUEST_URL, 60000);
            ht.debug = true;
            ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");

            return ht;
        }
    }



}