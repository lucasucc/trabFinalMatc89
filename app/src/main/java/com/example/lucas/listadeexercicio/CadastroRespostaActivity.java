package com.example.lucas.listadeexercicio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class CadastroRespostaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_resposta);

        String[] values3 = new String[] { "parte 1 da resposta", "parte 2 da respostas"};
        ArrayAdapter<String> itens3 = new ArrayAdapter<String>(this,R.layout.item_listview_lista,R.id.txtTituloLista,values3);
        ListView respostas = (ListView)findViewById(R.id.listView6);
        respostas.setAdapter(itens3);

    }

    public void chamarActivityCamera(View view){
        Intent intent2 = new Intent(this,CameraActivity.class);
        startActivity(intent2);
    }



}
