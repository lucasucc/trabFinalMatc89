package com.example.lucas.listadeexercicio;

import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.lucas.dao.DaoUsuario;
import com.example.lucas.model.Lista;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private Path pathsDoApp;
    private EditText tagBuscado;
    private ProgressBar progressBar2;
    private ListView listaView;
    private List<Lista> listas;
    private List listasItens;
    ArrayAdapter<String> listasArrayAdapter;
    private DaoUsuario daoUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.daoUsuario = new DaoUsuario(this);
        //this.daoUsuario.detelarUsuario();
        if(!this.daoUsuario.usuarioTemId()){
            Intent intent = new Intent(this,CadastroActivity.class);
            startActivity(intent);
        }
        this.listasItens = new ArrayList<String>();
        //this.listasItens.add("Lista 1");
        this.listas = new ArrayList<Lista>();
        this.listasArrayAdapter = new ArrayAdapter<String>(this,R.layout.item_listview_lista,R.id.txtTituloLista,this.listasItens);
        this.listaView = (ListView)findViewById(R.id.listView2);

        this.pathsDoApp = new Path();


        listaView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("id ="+id+"; position = "+position);
                chamarDetalheLista(listas.get(position).getIdLista());
                //MyAsyncTaskQuestoesLista questoesLista = new MyAsyncTaskQuestoesLista();
                //questoesLista.execute();

            }
        });

        listaView.setAdapter(this.listasArrayAdapter);
        this.tagBuscado = (EditText)findViewById(R.id.editText6);
        this.progressBar2 = (ProgressBar)findViewById(R.id.progressBar2);

    }

    private void prepararArquivosPadrao(){
        File pastaPadraoSalvamentoDaImagem = new File(pathsDoApp.getCaminhoParaIconeImagemPrincipal());//new File(Environment.getExternalStorageDirectory(), "ListaDeExercicio/"+pastaPadraoConteudo);

        pastaPadraoSalvamentoDaImagem.mkdirs();
    }


    public void chamarActivityCamera(View view){
        Intent intent2 = new Intent(this,CameraActivity.class);
        startActivity(intent2);
    }

    public void limpar(View view){
        this.tagBuscado.setText(" ");
    }

    public void buscar(View view){
        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute(this.tagBuscado.getText().toString());
        this.tagBuscado.setText("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,CadastroActivity.class);
            startActivity(intent);
        }else if( id == R.id.action_settings_cadastro_lista ){
            Intent intent = new Intent(this,CadastroListaExercicioActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    public void chamarDetalheLista(String idLista){
        Log.i("chamarDetalheLista","funcionou");
        Bundle bundle = new Bundle();
        bundle.putString("idLista",idLista);
        Intent intent = new Intent(this,DetalheListaActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }



    /*
    @Override
    public void onClick(View v) {
        Log.i("onclick","clicou");
        //chamarDetalheQuestao(v);
    }
    */

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public File gerarImagem(String base64,String idFotografia){
        byte[] imagemByte = Base64.decode(base64,Base64.DEFAULT);
        File pasta = new File(this.pathsDoApp.getCaminhoCompletoAtePastaPadrao());
        pasta.mkdirs();
        File file = new File(this.pathsDoApp.getCaminhoCompletoAtePastaPadrao() +"/"+ idFotografia + ".jpg");
        try (OutputStream imageOut = new FileOutputStream(file.getPath()) ){
            imageOut.write(imagemByte);
            imageOut.close();

            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void atualizarListas(Lista lista){
        listas.add(lista);
        listasItens.add("Lista: "+lista.getTitulo());
        listasArrayAdapter.notifyDataSetChanged();
    }

    public void limparTodasListas(){
        listas.clear();
        listasItens.clear();
        listasArrayAdapter.clear();
        listasArrayAdapter.notifyDataSetChanged();
    }
    public void criarObjetosListas(String[] listasDoServico ){
        for ( String listaDoServico:listasDoServico) {
            String[] StringPosicao = listaDoServico.split(";");
            Lista lista = new Lista();
            lista.setIdLista(StringPosicao[0].replace("[","").trim());
            lista.setTitulo(StringPosicao[1].trim());
            lista.setImagem( gerarImagem( StringPosicao[2],lista.getIdLista()));
            lista.setNomeDoArquivo(lista.getIdLista()+".jpg");
            atualizarListas(lista);
        }
    }
    public int obterQuantidadeDeListas(String conteudoListas) {
        int count = 0;
        for (int i = 0; i < conteudoListas.length(); i++) {
            if( conteudoListas.charAt(i)==']' ){
                count+=1;
            }
        }
        return count;
    }

    private class MyAsyncTask extends AsyncTask<String, Void, String> {

        private static final String NAMESPACE = "http://service/";
        private static final String MAIN_REQUEST_URL = "http://192.168.43.24:8082/WebServerListaExercicio/services/ServicePort";
        //private static final String METHOD_NAME = "multiplicar";
        //private static final String ARG_NAME0 = "arg0";
        //private static final String ARG_NAME1 = "arg1";


        @Override
        protected void onPreExecute() {
            progressBar2.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            //Log.i("nome do metodo =",params[0]);
            SoapObject soapObject = new SoapObject(NAMESPACE, "consultaListasTag");

            soapObject.addProperty("arg0",params[0]);

            HttpTransportSE httpTransport = getHttpTransportSE();
            soapObject.getProperty(0);
            SoapSerializationEnvelope envelope = getSoapSerializationEnvelope(soapObject);

            try {
                httpTransport.call("", envelope);

                return envelope.getResponse().toString();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            limparTodasListas();
            System.out.print("response="+response);
            Log.i("onPostExecute","talvez; "+response);
            if(response.equals("anyType{}")) {
                Log.i("onPostExecute","eh any type");
            }else{
                Log.i("onPostExecute","não eh any type");
                criarObjetosListas(response.split("]"));
            }
            /*
            String[] retorno = response.split("]");

            Log.i("AULA", "#############################");
            String nomeDaLista = "não retornou nada";

            Log.i("AULA ====", String.valueOf(retorno.length));
            if(retorno.length >= 2) {
                nomeDaLista = retorno[1];
            }
            Log.i("AULA",nomeDaLista );
            Log.i("AULA", "#############################");

            listasItens.add(nomeDaLista);
            listasArrayAdapter.notifyDataSetChanged();
            */
            Toast.makeText(getApplicationContext(),"Todas as listas disponíveis carregadas",Toast.LENGTH_LONG).show();
            progressBar2.setVisibility(View.INVISIBLE);
        }

        private final SoapSerializationEnvelope getSoapSerializationEnvelope(SoapObject soapObject) {

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.implicitTypes = true;
            envelope.setAddAdornments(false);

            envelope.setOutputSoapObject(soapObject);

            return envelope;
        }

        private final HttpTransportSE getHttpTransportSE() {

            HttpTransportSE ht = new HttpTransportSE(Proxy.NO_PROXY, MAIN_REQUEST_URL, 60000);
            ht.debug = true;
            ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");

            return ht;
        }
    }


    private class MyAsyncTaskQuestoesLista extends AsyncTask<String, Void, String> {

        private static final String NAMESPACE = "http://service/";
        private static final String MAIN_REQUEST_URL = "http://192.168.43.24:8082/WebServerListaExercicio/services/ServicePort";
        //private static final String METHOD_NAME = "multiplicar";
        //private static final String ARG_NAME0 = "arg0";
        //private static final String ARG_NAME1 = "arg1";

        @Override
        protected void onPreExecute() {
            progressBar2.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            Log.i("nome do metodo =",params[0]);
            SoapObject soapObject = new SoapObject(NAMESPACE, "consultaQuestoesListaTag");

            soapObject.addProperty("arg0",params[0]);
            soapObject.addProperty("arg1","");

            HttpTransportSE httpTransport = getHttpTransportSE();

            SoapSerializationEnvelope envelope = getSoapSerializationEnvelope(soapObject);

            try {
                httpTransport.call("", envelope);

                return envelope.getResponse().toString();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            progressBar2.setVisibility(View.INVISIBLE);
            Log.i("AULA", "#############################");
            Log.i("AULA", response);
            Log.i("AULA", "#############################");

            Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
            //chamarDetalheLista();
        }

        private final SoapSerializationEnvelope getSoapSerializationEnvelope(SoapObject soapObject) {

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.implicitTypes = true;
            envelope.setAddAdornments(false);

            envelope.setOutputSoapObject(soapObject);

            return envelope;
        }

        private final HttpTransportSE getHttpTransportSE() {

            HttpTransportSE ht = new HttpTransportSE(Proxy.NO_PROXY, MAIN_REQUEST_URL, 60000);
            ht.debug = true;
            ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");

            return ht;
        }
    }


}
