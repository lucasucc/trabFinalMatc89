package com.example.lucas.listadeexercicio;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.lucas.dao.DaoUsuario;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.Proxy;
import java.util.concurrent.CopyOnWriteArrayList;

public class CadastroActivity extends AppCompatActivity {


    private EditText nome;
    private EditText nomeUsuario;
    private DaoUsuario daoUsuario;
    private ProgressBar progressBar3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        this.nome = (EditText) findViewById(R.id.editText);
        this.nomeUsuario  = (EditText) findViewById(R.id.editText2);
        this.daoUsuario = new DaoUsuario(this);
        this.progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);

        String[] dadosUsuario = this.daoUsuario.obterDadosUsuario();

        if( dadosUsuario != null){
            this.nome.setText(dadosUsuario[1]);
            this.nomeUsuario.setText(dadosUsuario[2]);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cadastro, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            Intent intent = new Intent(this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //startActivityForResult(intent,1);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void cadastrarUsuario(View veiew){
        if(this.daoUsuario.usuarioTemId()){
            daoUsuario.updateDadoUsuario(this.nome.getText().toString(),this.nomeUsuario.getText().toString());
            Toast.makeText(this,"dados atualizados",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }else{
            MyAsyncTask myAsyncTask = new MyAsyncTask(this);
            myAsyncTask.execute(this.nome.getText().toString(),this.nomeUsuario.getText().toString());
        }
    }


    private class MyAsyncTask extends AsyncTask<String, Void, String> {

        private static final String NAMESPACE = "http://service/";
        private static final String MAIN_REQUEST_URL = "http://192.168.43.24:8082/WebServerListaExercicio/services/ServicePort";
        //private static final String METHOD_NAME = "multiplicar";
        //private static final String ARG_NAME0 = "arg0";
        //private static final String ARG_NAME1 = "arg1";
        private Context context;

        private MyAsyncTask(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            Log.i("nome do metodo =",params[0]);
            SoapObject soapObject = new SoapObject(NAMESPACE, "cadastroUsuario");

            soapObject.addProperty("arg0",params[0]);
            soapObject.addProperty("arg1",params[1]);


            /*
            if( obterArg0() == null && obterArg1() == null) {
                return null;
            }
            soapObject.addProperty(ARG_NAME0,obterArg0());
            soapObject.addProperty(ARG_NAME1,obterArg1());
            */
            HttpTransportSE httpTransport = getHttpTransportSE();

            SoapSerializationEnvelope envelope = getSoapSerializationEnvelope(soapObject);

            try {
                httpTransport.call("", envelope);

                return envelope.getResponse().toString();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            progressBar3.setVisibility(View.INVISIBLE);
            Log.i("AULA", "#############################");
            Log.i("AULA", response);
            Log.i("AULA", "#############################");

            daoUsuario.inserir(nome.getText().toString(),nomeUsuario.getText().toString(),response);
            Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this.context,MainActivity.class);
            startActivity(intent);
        }

        private final SoapSerializationEnvelope getSoapSerializationEnvelope(SoapObject soapObject) {

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.implicitTypes = true;
            envelope.setAddAdornments(false);

            envelope.setOutputSoapObject(soapObject);

            return envelope;
        }

        private final HttpTransportSE getHttpTransportSE() {

            HttpTransportSE ht = new HttpTransportSE(Proxy.NO_PROXY, MAIN_REQUEST_URL, 60000);
            ht.debug = true;
            ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");

            return ht;
        }
    }




}
