package com.example.lucas.listadeexercicio;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

public class DetalheQuestaoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_questao);

        String[] values3 = new String[] { "resposta 1", "resposta 2", "resposta 3"};
        ArrayAdapter<String> itens3 = new ArrayAdapter<String>(this,R.layout.item_listview_lista,R.id.txtTituloLista,values3);
        ListView respostas = (ListView)findViewById(R.id.listView5);
        respostas.setAdapter(itens3);

    }

    public void chamarCadastroResposta(View view){
        Intent intent = new Intent(this,CadastroRespostaActivity.class);
        startActivity(intent);
    }

    public void expandirImagem(View view){
        Log.i("expandir",String.valueOf(Environment.getExternalStorageDirectory()+"/ListaDeExercicio/images.png"));
        String path = Environment.getExternalStorageDirectory()+"/ListaDeExercicio/images.png";
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://"+ path), "image/*");
        startActivity(intent);
    }

}
